var moment = require('moment');
const aws = require("aws-sdk");
const aws_utils = require("../utils/aws-utils.js");

const S3 = new aws.S3({
  region: 'us-west-2',
});

export const contactuallyLambda = (event, context) => {
    const record = event.Records[0];
    
      var params = {
        Bucket: record.s3.bucket.name,
        Key: record.s3.object.key
      };
    // const data = await s3.getObject(params).promise();

    // const response = {
    //   statusCode: 200,
    //   body: JSON.stringify(data),
    // };
    // return response;

      //Fetch or read data from aws S3
      S3.getObject(params, async (err, data) => {
         console.log("s3.getObject");
        
        if (err) {
           console.log(err);
        } else {
          //this will log data to console
          if (data != undefined) {
            let filename = "contacts/formatted/" + moment().format("YYYY-MM-DD-h-mm-ss-a") + ".json";
            await data_formatter(JSON.parse(data.Body.toString()), filename);
          }
        }

      });
    
};

var data_formatter = async (data, filename) => {
  console.log("data ::::::: ", Array.isArray(data.contacts));
  
  let formatted_data = [];
  for (let contact of data.contacts) {

    let contact_addresses_data = [];
    for (let address of contact.addresses) {
      let address_data = {
        label: address.label,
        street_1: address.street_1,
        street_2: address.street_2,
        city: address.city,
        state: address.state,
        zip: address.zip,
        country: address.country
      };
      contact_addresses_data.push(address_data);
    }
    let contact_email_addresses_data = [];
    for (let email_address of contact.email_addresses) {
      let email_address_data = {
        label: email_address.label,
        address: email_address.address
      };
      contact_email_addresses_data.push(email_address_data);
    }
    let contact_phone_numbers_data = [];
    for (let phone_number of contact.phone_numbers) {
      let phone_number_data = {
        label: phone_number.label,
        number: phone_number.number
      };
      contact_phone_numbers_data.push(phone_number_data);
    }
    let contact_data = {
      first_name: contact.first_name,
      last_name: contact.last_name,
      company: contact.company,
      addresses: contact_addresses_data,
      email_addresses: contact_email_addresses_data,
      phone_numbers: contact_phone_numbers_data
    };
    let contact_obj = {
      source: "contactually-lambda",
      source_object_id: contact.id,
      object_data: contact_data
    };
    formatted_data.push(contact_obj);
  }
  let json_data = JSON.stringify({
    contacts: formatted_data
  });
  aws_utils.pushContacts(process.env.BUCKET, json_data, filename);
};

