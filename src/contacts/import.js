const axios = require("axios");
var moment = require('moment');
const aws = require("aws-sdk");
const aws_utils = require("../utils/aws-utils.js");
var request = require("request");
const constants = require("../utils/constants.js");


const s3 = new aws.S3({
  region: 'us-west-2',
});

export const contactually = async (event, context) => {
  var contact_type = "contactually-lambda/";
  var user_hash = event.body;
  let integration = null;
  try {
  let env = process.env.STAGE;
  await registerForIntegrationSetting(env)
      .then(function (result) {
        console.log("Registration success and received access token");
        return getIntegrationsList(result.auth, result.constants);
      }).then(function (result) {
        try{
          integration = result.integrationList.filter(integration => integration.integration_short_name.toLowerCase() === "contactually");
          return getIntegrationConnection(result.auth, result.constants, integration[0].integration_id);
        }catch(error){
          console.log(error);
          reject("Integration error");
        }
      }).then(async function (result) {
        //watch: integration connection needs to be created prior
        if (result.integration_connection.integration_connection_status === 'active'){
          let access_token = result.integration_connection.key_value.access_token;
          await getContactuallyContacts(access_token, contact_type);
        }else{
          reject("Integration Connection not active");
        }

      },
      function (err) {
        console.log(err);
      });
  } catch (error) {
    console.log(error);
  }


  return {
    statusCode: 200, 
    body: JSON.stringify({
      success: true
    }),
  };
};

//Get authentication token for integration settings registration
var registerForIntegrationSetting = async (env) => {
  return new Promise(async function (resolve, reject) {
    try {
      let constantsData = new constants(env);
      console.log("env :" + env);
      var options = {
        method: 'POST',
        url: constantsData.iRegisterUrl,
        headers: {
          "Content-Type": "application/json",
          "x-api-key": constantsData.iApikey
        },
        body: {
          app_name: process.env.PROJECT
        },
        json: true
      };
      
      await request(options, async function (error, response, body) {
        if (error || body.message === 'Internal server error') {
          reject(new Error(error));
        } else {
          var resultData = {
            auth: body.auth_token,
            constants: constantsData,
            status: "success"
          };
          resolve(resultData);
        }
      });
    } catch (error) {
      reject(error);
    }
  });
};

//Fetch Integrations list
var getIntegrationsList = async (authToken, constantsData) => {
  return new Promise(async function (resolve, reject) {
    try {
      var options = {
        method: 'GET',
        url: constantsData.iIntegrationsListUrl,
        headers: {
          'cache-control': 'no-cache',
          Authorization: authToken
        },
        json: true
      };
      await request(options, function (error, response, body) {
        if (error) {
          reject(new Error(error));
        } else {
          var integrationList = body;
          if (integrationList != null && integrationList.length > 0) {
            var resultData = {
              auth: authToken,
              constants: constantsData,
              status: "success",
              integrationList: integrationList
            };
            resolve(resultData);
          } else {
            reject("Integrations not present");
          }
        }
      });
    } catch (error) {
      reject(error);
    }
  });
};

//Fetch clients/connections for the integration
var getIntegrationConnection = async (authToken, constantsData, integrationId) => {
  return new Promise(async function (resolve, reject) {
    try {
      var options = {
        method: 'GET',
        url: constantsData.iIntegrationConnectionsUrl + "/" + authToken + ":" + integrationId,
        headers: {
          'cache-control': 'no-cache',
          Authorization: authToken
        },
        json: true
      };
      // console.log("IC::options ", options);

      await request(options, function (error, response, body) {
        if (error || body.message === 'Internal server error') {
          reject(new Error(error));
        } else {
          // console.log("IC:: body ", body);
          
          if (body.auth_token) {
            var resultData = {
              auth: authToken,
              constants: constantsData,
              status: "success",
              integration_connection: body
            };
            resolve(resultData);
          } else {
            reject("Integration Connections not present");
          }
        }
      });
    } catch (error) {
      reject(error);
    }
  });
};


var getContactuallyContacts = async (access_token, contact_type) => {
  console.log("access_token, contact_type::", access_token, contact_type);
    try {
        await axios({
          method: "GET",
          url: "https://api.contactually.com/v2/contacts",
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + access_token
          }
        })
        .then(async response => {
            console.log("Contactually data:: ", response.data);
            
            let filename = "contacts/import/" + contact_type + moment().format("YYYY-MM-DD-h-mm-ss-a") + ".json";
            let data = JSON.stringify({
              contacts: response.data.data
            });
            console.log("bucket, data, filename:: ", process.env.BUCKET, data, filename);
            
            await aws_utils.pushContacts(process.env.BUCKET, data, filename);
        }).catch(err => {
          console.log("caught: Access Token:Error::" + err);
        });
    } catch (error) {
      console.log(error);
    }
};
