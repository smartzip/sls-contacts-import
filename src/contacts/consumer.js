import uuid from "uuid";
import * as dynamoDbLib from "../../libs/dynamodb-lib";
const CONTACTS_IMPORT_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-ContactsImportTable`;
const aws = require("aws-sdk");

const s3 = new aws.S3({
  region: 'us-west-2',
});

export const contactsConsumer = (event, context) => {
  const record = event.Records[0];

  var params = {
    Bucket: record.s3.bucket.name,
    Key: record.s3.object.key
  };

  //Fetch or read data from aws s3
  s3.getObject(params, (err, data) => {
    console.log("s3.getObject");

    if (err) {
     console.log(err);
    } else {
      //this will log data to console
      let contacts = JSON.parse(data.Body.toString()).contacts;
      if (contacts != undefined) {
        for (let contact of contacts) {
          saveContacts(contact.source, contact.source_object_id, JSON.stringify(contact.object_data));
        }
      }
    }

  });

};


var saveContacts = async (source, source_object_id, object_data) => {
  const params = {
    TableName: CONTACTS_IMPORT_TABLE,
    Item: {
      contact_id: uuid.v1(),
      source: source,
      source_object_id: source_object_id,
      object_data: object_data,
      modified_at: new Date().toJSON()
    }
  };
  
  try {
    await dynamoDbLib.call("put", params);
    console.log("contacts created ");
    return params.Item;
  } catch (e) {
    console.log("Error :: " + e);
    return failure({
      status: false
    });
  }
};