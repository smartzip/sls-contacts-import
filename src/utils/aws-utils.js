const aws = require("aws-sdk");

const s3 = new aws.S3({
  region: 'us-west-2',
});

// Create an S3 bucket "sls-contacts-import-dev/staging/prod" prior to running these functions
var pushContacts = (bucket, data, filename) => {
  try {
    var params = {
      Body: data,
      Bucket: bucket,
      Key: filename
    };
    console.log("pushContacts:");

    s3.putObject(params, function (err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else console.log(data); // successful response
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  pushContacts
};