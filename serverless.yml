# NOTE: update this with your service name
service: sls-contacts-import

# Use the serverless-webpack plugin to transpile ES6
plugins:
  - serverless-webpack
  - serverless-offline

# serverless-webpack configuration
# Enable auto-packing of external modules
custom:
  webpack:
    webpackConfig: ./webpack.config.js
    includeModules: true
  stage: ${opt:stage, self:provider.stage}
  tableThroughputs:
    prod: 5
    staging: 5
    default: 1
  tableThroughput: ${self:custom.tableThroughputs.${self:custom.stage}, self:custom.tableThroughputs.default}
  bucket: sls-contacts-import-${opt:stage, self:provider.stage}

provider:
  name: aws
  runtime: nodejs8.10
  stage: dev
  region: us-west-2
  # To load environment variables externally
  # rename env.example to env.yml and uncomment
  # the following line. Also, make sure to not
  # commit your env.yml.
  #
  #environment: ${file(env.yml):${self:provider.stage}}
  environment:
    STAGE: ${opt:stage, self:provider.stage}
    PROJECT: ${self:service}-${opt:stage, self:provider.stage}
    DYNAMODB_TABLE_PREFIX: ${self:service}-${opt:stage, self:provider.stage}
  iamRoleStatements:
    - Effect: Allow
      Action:
        - lambda:InvokeFunction
      Resource: "*"
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
        - dynamodb:ListStreams
        - dynamodb:DescribeTable
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.DYNAMODB_TABLE_PREFIX}*"
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.DYNAMODB_TABLE_PREFIX}*/index/*"
    - Effect: Allow
      Action:
        - s3:*
        # - s3:PutObject
        # - s3:PutObjectAcl
        # - s3:GetObject
        # - s3:ListBucket
      Resource: "arn:aws:s3:::${self:custom.bucket}/*"
    - Effect: Allow
      Action:
        - s3:*
      Resource: "arn:aws:s3:::*/*"

functions:
  contactsImportContactually:
    handler: src/contacts/import.contactually
    events:
      - http:
          path: contactually/import-contacts
          method: get
          cors: true
      # - schedule: 
      #     rate: cron(0 12 * * ? *)
      #     enabled: false
    environment:
      BUCKET: ${self:custom.bucket}

  contactsFormatter:
    handler: src/contacts/formatter.contactuallyLambda
    events:
      - s3:
          bucket: ${self:custom.bucket}
          event: s3:ObjectCreated:*
          rules:
            - prefix: contacts/import/contactually-lambda/
            - suffix: .json
    environment:
      BUCKET: ${self:custom.bucket}
  contactsConsumer:
    handler: src/contacts/consumer.contactsConsumer
    events:
      - s3:
          bucket: ${self:custom.bucket}
          event: s3:ObjectCreated:*
          rules:
            - prefix: contacts/formatted/
            - suffix: .json
  contactsProcess:
    handler: src/contacts/process.contactsProcess
    events:
      - stream:
          type: dynamodb
          batchSize: 1
          startingPosition: LATEST
          arn:
            Fn::GetAtt:
              - slsContactsImportTable
              - StreamArn
resources:
  Resources:
    slsContactsImportTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        AttributeDefinitions:
          -
            AttributeName: contact_id
            AttributeType: S
        KeySchema:
          -
            AttributeName: contact_id
            KeyType: HASH
        # Set the capacity based on the stage
        ProvisionedThroughput:
          ReadCapacityUnits: ${self:custom.tableThroughput}
          WriteCapacityUnits: ${self:custom.tableThroughput}
        TableName: ${self:provider.environment.DYNAMODB_TABLE_PREFIX}-ContactsImportTable
        StreamSpecification:
          StreamViewType: NEW_IMAGE
              
  Outputs:
    ContactsImportArn:
      Value:
         Fn::GetAtt:
          - slsContactsImportTable
          - Arn
      Export:
        Name: ${self:provider.environment.DYNAMODB_TABLE_PREFIX}-ContactsImportTableArn